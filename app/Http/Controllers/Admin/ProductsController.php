<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Product;
use Intervention\Image\Facades\Image;
use Gate;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = auth()->user()->name;

        $products = Product::where('seller_name', $user)->paginate(2);

        //$products =  \DB::table('products')->where('seller_name', 'Admin User');
        return view('admin.sellers.index')->with('products',$products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sellers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,User $user)
    {
        $data = request()->validate([
            'name' => 'required',
            'desc' => 'required',
            'price' => ['required','integer'],
            'seller_name' => '',
            'image' => ['required'],

        ]);
        $ImagePath = request('image')->store('uploads','public');
        $image = Image::make(public_path("storage/{$ImagePath}"))->fit(200,200);
        $image->save();

        $poster = auth()->user()->name;
        $poster_id = auth()->user()->id;

        $products = auth()->user()->products()->create([
            'name' => $request['name'],
            'image' => $ImagePath,
            'price' => $request['price'],
            'desc' => $request['desc'],
            'seller_name' => $poster,
            

        ]);


        return redirect(route('admin.sellers.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, $products)
    {
        if(Gate::denies('edit-users')){
            return redirect(route('admin.users.index'));
        }
        
        $product = Product::where('id', $products)->first();

        return view('admin.sellers.edit')->with([
            'product' => $product,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

     
        /*
        $data = auth()->user()->products();
        dd($data);
        $user ->products()->sync($request->products);
        if($user->save()){
            $request->session()->flash('success','  updated');
        }else{
            $request->session()->flash('error','error');
        }

        */

        $data = Product::where('id',$id)->first();

        $data->name = $request->name;
        $data->desc = $request->desc;

        $data->save();

        return redirect(route('admin.sellers.index'));


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, $id,Request $request)
    {
        if(Gate::denies('manage-users')){
            return redirect(route('admin.users.index'));
        }
        $data = Product::where('id',$id)->first();
        $data->delete();
        $request->session()->flash('error','delete success');
        return redirect()->route('admin.sellers.index');
    }
}
