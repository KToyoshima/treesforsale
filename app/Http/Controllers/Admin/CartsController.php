<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Cart;
use App\Product;
use Illuminate\Http\Request;

class CartsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $products = Product::all();

        //$products =  \DB::table('products')->where('seller_name', 'Admin User');
        return view('admin.carts.index')->with('products',$products);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user,$id, Request $request)
    {
        //add new data 
        //id is the ID of the product 



        $data = $id;
        
        $products = auth()->user()->carts()->create([

            'product' => $data

        ]);

        $products = Product::all();
        $request->session()->flash('success','Product Added to Cart!');
       //return view('admin.carts.index')->with('products',$products);
        return redirect()->route('admin.carts.index');
    }

 
}
