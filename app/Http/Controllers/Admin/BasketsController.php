<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Cart;
use App\Product;
use Illuminate\Http\Request;

class BasketsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user()->id;
        $products = Cart::where('user_id', $user)->get();
        
        $items = Cart::all('product')->toArray();
        
        $test = array();
        foreach($items as $item){
            $test[] = Product::where('id', $item)->first();
        }

        
       $things = Product::all();

        



 
        //$products =  \DB::table('products')->where('seller_name', 'Admin User');
        return view('admin.baskets.index',compact('things'))->with('products',$products);
    }

   

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user,$id,Request $request)
    {

        $data = Cart::where('id',$id);

        $data->delete();

        $request->session()->flash('error','Data Deleted');
        return redirect()->route('admin.baskets.index');
    }
}
