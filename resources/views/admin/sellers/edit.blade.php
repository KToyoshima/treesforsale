@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Product</div>

                <div class="card-body">
                    <form action="{{route('admin.sellers.update',$product)}}" method="POST">
                        {{method_field('PUT')}}
                        @csrf
                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $product->name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-md-2 col-form-label text-md-right">Price</label>

                                <div class="col-md-6">
                                    <input id="price" type="price" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ $product->price }}" required autocomplete="price" autofocus>

                                    @error('price')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                     

                            <div class="form-group row">
                                <label for="desc" class="col-md-2 col-form-label text-md-right">Description</label>

                                <div class="col-md-6">
                                    <input id="desc" type="desc" class="form-control @error('desc') is-invalid @enderror" name="desc" value="{{ $product->desc }}" required autocomplete="desc" autofocus>

                                    @error('desc')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>



                       
                        <button type="submit" class="btn btn-primary">
                            Update
                        </button>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
