@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> <a href="{{route('admin.sellers.create')}}"><button type="button" class="btn btn-success float-left">Add New Product</button></a></div>
                <div class="card-body">

                    <table class="table">
                        <thead>
                          <tr>
                  
                            <th scope="col">Product Name</th>
                            <th scope="col">Seller </th>
                            <th scope="col">Image </th>
                            <th scope="col">Actions </th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)


                                <tr>
                                    <th scope="row">{{$product->name}}</th>
                                    
                                    <td>{{$product->seller_name}}</td>
                                    <td><img src="/storage/{{$product->image}}" alt="w-100"></td>
                                    <td>
                                        @can('edit-users')
                                            <a href="{{route('admin.sellers.edit',$product->id)}}"><button type="button" class="btn btn-success float-left">Edit</button></a>
                                        @endcan
                                        <form action="{{route('admin.sellers.destroy', $product->id)}}" method="POST" class="float-left">
                                            @csrf
                                            {{method_field('DELETE')}}
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                      <span>{!! $products->render() !!}</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

