@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Product Page</div>
                    <div class="card-body">

                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">Product Name</th>
                            <th scope="col">Image </th>
                            <th scope="col">Price </th>
                            <th scope="col">Actions </th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)


                                <tr>
                                    <th scope="row">{{$product->name}}</th>
                                    <td><img src="/storage/{{$product->image}}" alt="w-100"></td>
                                    <td>{{$product->price}} PHP</td>
                                    <td>
                                        
                                        <a href="{{route('admin.carts.edit',$product->id)}}"><button type="button" name=data class="btn btn-success float-left">Add To Cart</button></a>
                                       @can('manage-users')
                                        <form action="{{route('admin.sellers.store', $product->id)}}" method="POST" class="float-left">
                                            @csrf
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                      
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

