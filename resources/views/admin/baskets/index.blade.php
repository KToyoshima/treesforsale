@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">User</div>
                    <div class="card-body">

                    <table class="table">
                        <thead>
                          <tr>
                            
                            <th scope="col">Product Name</th>
                            <th scope="col">Price </th>
                            <th scope="col">Action </th>
                          </tr>
                        </thead>
                        <tbody>
                          
                            @foreach ($products as $product)
                         
                                


                                <tr>
                                    <th scope="row">{{$things[$product->product - 1]->name}}</th>
                                    <td>{{$things[$product->product - 1]->price}} PHP</td>
                                    <td>

                                        <form action="{{route('admin.baskets.destroy', $product->id)}}" method="POST" class="float-left">
                                            @csrf
                                            {{method_field('DELETE')}}
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                              
                            @endforeach
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

